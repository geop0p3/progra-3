#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL

void inicializar(void);
using namespace std;
int W=800, H=600;
float aspect = (float)W/(float)H;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;

int counter = 1000;

float cameraAngleX = 0;
float cameraAngleY = 0;

//Viewport
float   clipAreaL,
        clipAreaR,
        clipAreaT,
        clipAreaB;

//Lights
float speed = 0.01;

float ambientColor[] = {0.2f, 0.2f, 0.2f, 1.0f};
float diffuseColor[] = {0.8f, 0.8f, 0.8f, 1.0f};
float specularColor[] = {1.0f, 1.0f, 1.0f, 1.0f};
float cR[] = {1.0, 0.0, 0.0, 1.0};
float cG[] = {0.0, 1.0, 0.0, 1.0};
float cB[] = {0.0, 0.0, 1.0, 1.0};
float cY[] = {1.0, 1.0, 0.0, 1.0};

float luzRojo[] = {10.0, 5.0, 0.0f, 1.0f};
float luzVerde[] = {-10.0, 5.0, 0.0f, 1.0f};
float luzAzul[] = {10.0, -5.0, 0.0f, 1.0f};
float luzAmarillo[] = {-10.0, -5.0, 0.0f, 1.0f};

//Sphere
GLUquadricObj *m_Sphere;

void Set_Lights()
{
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, cR);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT0, GL_POSITION, luzRojo);

}


void Move_Spheres(float s)
{
    glRotatef(TimeStart*s, 0.0, 2.0, 1.0);
}

void reshape(int w, int h)
{
    H=(h==0)? 1:h;
    W=(w==0)? 1:w;

    glViewport(0, 0, W, H);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    aspect = (float)W/(float)H;

    if(W >= H)
    {
        clipAreaL = -1.0 * aspect;
        clipAreaR = 1.0 * aspect;
        clipAreaB = -1.0;
        clipAreaT = 1.0;
    }
    else
    {
        clipAreaL = -1.0;
        clipAreaR = 1.0;
        clipAreaB = -1.0 / aspect;
        clipAreaT = 1.0 / aspect;
    }
    gluOrtho2D(clipAreaL, clipAreaR, clipAreaB, clipAreaT);

    //gluPerspective(40.0f, (GLfloat)W/(GLfloat)H, 1.0f, 1000.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void inicializar(void)
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    reshape(W, H);
    glEnable(GL_DEPTH_TEST); //Profundidad

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Activate Lights
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);


    //Set Light Colors/Position
    Set_Lights();

    //Sphere
    m_Sphere = gluNewQuadric();

    TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
    //Time
    TimeStart = glutGet(GLUT_ELAPSED_TIME);
    counter -= deltaTime;

    //Counter reset
    if(counter <=0 ) counter = 1000;

    glutPostRedisplay();
}

void render(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glDisable(GL_LIGHTING);
    glTranslatef(0.0, 0.0, 0.0);
    glColor3f(1.0, 1.0, 0.0);
    gluSphere(m_Sphere, 0.1, 40, 40);
    glEnable(GL_LIGHTING);
    glPushMatrix();
      // Planeta
        glColor3f(0.0, 0.0, 1.0);
        glRotatef(TimeStart*0.1, 0.0, 1.0, 0.2);
        glTranslatef(0.1, -0.1, 0.1);
        glRotatef(TimeStart, 0.0, 1.0, 0.2);
        gluSphere(m_Sphere, 0.06, 15, 15);

        glPushMatrix();
        // Luna
        glColor3f(1.0, 1.0, 1.0);
            glRotatef(TimeStart*0.01, 0.0, 1.0, 0.2);
            glTranslatef(0.07, -0.07, 0.07);
            glRotatef(TimeStart*0.05, 0.0, 0.4, 1.0);
            gluSphere(m_Sphere, 0.02, 15, 15);
        glPopMatrix();

        // Planeta 2
        glColor3f(0.0, 1.0, 1.0);
        glRotatef(TimeStart*0.02, 0.0, 1.0, 0.2);
        glTranslatef(0.4, 0.1, 0.1);
        glRotatef(TimeStart, 0.0, 1.0, 0.2);
        gluSphere(m_Sphere, 0.08, 15, 15);

        // Luna 2
        glPushMatrix();
        glColor3f(1.0, 1.0, 1.0);
            glRotatef(TimeStart*0.4, 0.0, 1.0, 0.2);
            glTranslatef(0.0, -0.07, 0.0);
            glRotatef(TimeStart, 0.0, 0.4, 1.0);
            gluSphere(m_Sphere, 0.02, 15, 15);
        glPopMatrix();

    glPopMatrix();

    //Tiempo
    deltaTime = TimeStart - TimeOld;
    TimeOld = TimeStart;

    glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
    //estados_teclas[key]=true;
    switch(key)
    {
        case 'w':
        case 'W': cameraAngleX --; break;

        case 's':
        case 'S': cameraAngleX ++; break;

        case 'a':
        case 'A': cameraAngleY --; break;

        case 'd':
        case 'D': cameraAngleY ++; break;

            exit(0);
    }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
    //estados_teclas[key]=false;
    switch(key)
    {
        case 'a':
        case 'A':
            //que ahce
            break;
    }
}

void eventosTecladoEspecial(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_F1:
            //que ahce al presionar f1
            break;
    }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_F1:
            //que ahce al presionar f1
            break;
    }
}

/*void analizar_teclas(void)
{
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
}*/

void eventosMouse(int button, int state, int x, int y)
{
    switch(state)
    {
        case GLUT_UP:
            switch(button)
            {
                case GLUT_LEFT_BUTTON:

                    break;
                case GLUT_RIGHT_BUTTON:
                    break;
                case GLUT_MIDDLE_BUTTON:
                    break;
            }
            break;
        case GLUT_DOWN:
            break;
    }
}

int main(int argc, char **argv)
{
    //inicamos el glut
    glutInit(&argc, argv);
    //Especificamos el modo de display
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    //Creamos la ventana
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(10, 150);
    glutCreateWindow("Aplicacion OpenGL con GLUT");

    glutIdleFunc(Idle);
    glutDisplayFunc(render);
    glutReshapeFunc(reshape);
    inicializar();

    ///Teclados y mouse
    //Los que revisan teclado
    glutKeyboardFunc(eventosTeclado);
    glutKeyboardUpFunc(eventosTecladoUp);
    glutSpecialFunc(eventosTecladoEspecial);
    glutSpecialUpFunc(eventosTecladoEspecialUp);
    //mouse
    glutMouseFunc(eventosMouse);

    //El ciclo infinito
    glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

    //Free memory
    if(m_Sphere) gluDeleteQuadric(m_Sphere);
    m_Sphere = NULL;

    return 0;
}

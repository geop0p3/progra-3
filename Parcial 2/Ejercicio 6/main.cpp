#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

void inicializar(void);
using namespace std;
int W=800, H=600;
float aspect = (float)W/(float)H;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;

int contador = 600;

float cameraAngleX = 0;
float cameraAngleY = 0;

float   clipAreaL,
        clipAreaR,
        clipAreaT,
        clipAreaB;

float speed = 0.01;

float ambientColor[] = {1.0f, 0.0f, 0.8f, 1.0f};
float diffuseColor[] = {0.4f, 0.2f, 0.2f, 1.0f};
float specularColor[] = {1.0f, 1.0f, 1.0f, 1.0f};
float Azul[] = {0.0, 0.0, 1.0, 1.0};
float amarillo[] = {1.0, 1.0, 0.0, 1.0};
float rojox[] = {1.0, 0.0, 0.0, 1.0};
float Verd[] = {0.0, 1.0, 0.0, 1.0};
float LR[] = {10.0, 5.0, 0.0f, 1.0f};
float LA[] = {10.0, -5.0, 0.0f, 1.0f};
float LV[] = {-10.0, 5.0, 0.0f, 1.0f};
float LAm[] = {-10.0, -5.0, 0.0f, 1.0f};

GLUquadricObj *m_Sphere;


void reshape(int w, int h)
{
    H=(h==0)? 1:h;
    W=(w==0)? 1:w;

    glViewport(0, 0, W, H);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    aspect = (float)W/(float)H;

    if(W >= H)
    {
        clipAreaL = -1.0 * aspect;
        clipAreaR = 1.0 * aspect;
        clipAreaB = -1.0;
        clipAreaT = 1.0;
    }
    else
    {
        clipAreaL = -1.0;
        clipAreaR = 1.0;
        clipAreaB = -1.0 / aspect;
        clipAreaT = 1.0 / aspect;
    }
    gluOrtho2D(clipAreaL, clipAreaR, clipAreaB, clipAreaT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void inicializar(void)
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    reshape(W, H);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    glEnable(GL_LIGHT3);

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, rojox);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT0, GL_POSITION, LR);

    glLightfv(GL_LIGHT1, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, Verd);
    glLightfv(GL_LIGHT1, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT1, GL_POSITION, LV);

    glLightfv(GL_LIGHT2, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, Azul);
    glLightfv(GL_LIGHT2, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT2, GL_POSITION, LA);

    glLightfv(GL_LIGHT3, GL_AMBIENT, ambientColor);
    glLightfv(GL_LIGHT3, GL_DIFFUSE, amarillo);
    glLightfv(GL_LIGHT3, GL_SPECULAR, specularColor);
    glLightfv(GL_LIGHT3, GL_POSITION, LAm);

    m_Sphere = gluNewQuadric();

    TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
    //Time
    TimeStart = glutGet(GLUT_ELAPSED_TIME);
    contador -= deltaTime;

    //contador reset
    if(contador <=0 ) contador = 600;

    glutPostRedisplay();
}

void render(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
  glRotatef(TimeStart*0.5, 0.0, 1.0, 0.0);
    for(int y = 0; y < 6; y++)
    {
        for(int x = 0; x < 4; x++)
        {
            glPushMatrix();
                glTranslatef((x/3.0)-0.5, (y/4.5)-0.5, 0.0);
                gluSphere(m_Sphere, 0.1, 15, 15);
            glPopMatrix();
        }
    }

    deltaTime = TimeStart - TimeOld;
    TimeOld = TimeStart;

    glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
    //estados_teclas[key]=true;
    switch(key)
    {
        case 'w':
        case 'W': cameraAngleX --; break;

        case 's':
        case 'S': cameraAngleX ++; break;

        case 'a':
        case 'A': cameraAngleY --; break;

        case 'd':
        case 'D': cameraAngleY ++; break;

            exit(0);
    }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
    //estados_teclas[key]=false;
    switch(key)
    {
        case 'a':
        case 'A':
            //que ahce
            break;
    }
}

void eventosTecladoEspecial(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_F1:
            //que ahce al presionar f1
            break;
    }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
    switch(key)
    {
        case GLUT_KEY_F1:
            //que ahce al presionar f1
            break;
    }
}

/*void analizar_teclas(void)
{
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
}*/

void eventosMouse(int button, int state, int x, int y)
{
    switch(state)
    {
        case GLUT_UP:
            switch(button)
            {
                case GLUT_LEFT_BUTTON:

                    break;
                case GLUT_RIGHT_BUTTON:
                    break;
                case GLUT_MIDDLE_BUTTON:
                    break;
            }
            break;
        case GLUT_DOWN:
            break;
    }
}

int main(int argc, char **argv)
{
    //inicamos el glut
    glutInit(&argc, argv);
    //Especificamos el modo de display
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    //Creamos la ventana
    glutInitWindowSize(800, 600);
    glutInitWindowPosition(10, 150);
    glutCreateWindow("Aplicacion OpenGL con GLUT");

    glutIdleFunc(Idle);
    glutDisplayFunc(render);
    glutReshapeFunc(reshape);
    inicializar();

    ///Teclados y mouse
    //Los que revisan teclado
    glutKeyboardFunc(eventosTeclado);
    glutKeyboardUpFunc(eventosTecladoUp);
    glutSpecialFunc(eventosTecladoEspecial);
    glutSpecialUpFunc(eventosTecladoEspecialUp);
    //mouse
    glutMouseFunc(eventosMouse);

    //El ciclo infinito
    glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

    //Free memory
    if(m_Sphere) gluDeleteQuadric(m_Sphere);
    m_Sphere = NULL;

    return 0;
}


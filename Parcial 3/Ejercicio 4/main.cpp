#include <iostream>
#include <stdio.h>
#include <time.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL
#include <math.h>
#include "CTargaImage.h"
#include "CTargaImage.cpp"

void inicializar(void);
using namespace std;
int W=800, H=600;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;
float C = 1000;

float clipAreaXLeft = 0;
float clipAreaXRight = 0;
float clipAreaYBottom = 0;
float clipAreaYTop = 0;

CTargaImage *m_TexturaUNO;
unsigned int m_ObjetoTexturaUNO;

int i=0;
int j=0;

void NewAspect(){
        float aspect = (float)W / (float)H;
        if(W >= H) {
                clipAreaXLeft = -1.0 * aspect;
                clipAreaXRight = 1.0 * aspect;
                clipAreaYBottom = -1.0;
                clipAreaYTop = 1.0;
        }
        else{
                clipAreaXLeft = -1.0f;
                clipAreaXRight = 1.0f;
                clipAreaYBottom = -1.0 / aspect;
                clipAreaYTop = 1.0 / aspect;
        }

        gluOrtho2D(clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop);
}

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(52.0f, (GLfloat)W/(GLfloat)H, 1.0f, 100.0f);
        //NewAspect();
        //glOrtho(0, W-1, 0, H-1, -1, 1);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST); //Profundidad

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        //Texturas
        glEnable(GL_TEXTURE_2D);
        m_TexturaUNO = new CTargaImage;

        if(!m_TexturaUNO->Load("quimeras_r3.tga"))
                return;

        glGenTextures(1, &m_ObjetoTexturaUNO);

        glBindTexture(GL_TEXTURE_2D, m_ObjetoTexturaUNO);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_TexturaUNO->GetWidth(), m_TexturaUNO->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, m_TexturaUNO->GetImage());

        TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
        TimeStart = glutGet(GLUT_ELAPSED_TIME);

        C -= deltaTime;
        glutPostRedisplay();
}

void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        //gluLookAt(0, 1.0f, 3.0f, 0, 0, 0, 0, 1.0f, 0);

        glPushMatrix();
        glTranslatef(0.0, 0.0, -5.0);
        glRotatef(90.0, 1.0, 0.0, 0.0);

        glBindTexture(GL_TEXTURE_2D, m_ObjetoTexturaUNO);

        glBegin(GL_TRIANGLE_STRIP);
        glTexCoord2f(2.0, 0.0); glVertex3f(2.0f, -2.0f, -2.0f);
        glTexCoord2f(0.0, 0.0); glVertex3f(-2.0f, -2.0f, -2.0f);
        glTexCoord2f(2.0, 2.0); glVertex3f(2.0f, -2.0f, 2.0f);
        glTexCoord2d(0.0, 2.0); glVertex3f(-2.0,-2.0,2.0);
        glEnd();
        glPopMatrix();

        //bi.ShowImage();
        //images.LoadImages();

        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                //images.PreviousImage(-1);
                break;

        case 's':
        case 'S':
                //images.NextImage(1);
                break;

        case 'q':
        case 'Q':
                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':

                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:
                        std::cout << x << "  "<< y << std::endl;
                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        //images.ReleaseImages();

        glDeleteTextures(1, &m_ObjetoTexturaUNO);
        m_TexturaUNO->Release();
        delete m_TexturaUNO;

        return 0;
}

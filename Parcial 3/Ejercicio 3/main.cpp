#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "CTargaImage.h"
#include "CTargaImage.cpp"

void inicializar(void);
using namespace std;
int W=800, H=600;
float aspect = (float)W/(float)H;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;

float cameraAngleX = 0;
float cameraAngleY = 0;

CTargaImage* img;


GLUquadricObj *m_Sphere;


void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        aspect = (float)W/(float)H;

        glOrtho(0,W-1,0,H-1, -1, 1);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        m_Sphere = gluNewQuadric();
        TimeOld = glutGet(GLUT_ELAPSED_TIME);

        img=new CTargaImage;
        img->Load("imagen.tga");
}

void Idle(void)
{
        glutPostRedisplay();
}


void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
        glRasterPos2i(0,100);
        glDrawPixels((int)img->GetWidth(), (int)img->GetHeight(), GL_RGB, GL_UNSIGNED_BYTE, img->GetImage());
        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'w':
        case 'W': cameraAngleX--; break;

        case 's':
        case 'S':
                img->Release();
                img->Load("imagen3.tga"); break;

        case 'a':
        case 'A':
                img->Release();
                img->Load("imagen2.tga"); break;

        case 'd':
        case 'D':
                img->Release();
                img->Load("imagen.tga"); break;

                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                //que ahce
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:

                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        //Free memory
        img->Release();
        delete img;


        return 0;
}

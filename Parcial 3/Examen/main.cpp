#include <iostream>
#include <stdio.h>
#include <cmath>
#include <vector>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "CTargaImage.h"
#include "CTargaImage.cpp"

void inicializar(void);
using namespace std;
int W=1200, H=800;
float aspect = (float)W/(float)H;

CTargaImage *m_Tex1;
CTargaImage *m_Tex2;
CTargaImage *m_Tex3;
CTargaImage *m_Tex4;
CTargaImage *m_Tex5;

unsigned int m_ObjTex1[5];


float equis = 0;
float ye = 0;
float x2 = equis + 0.5f;
float y2 = ye + 0.75f;

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        aspect = (float)W/(float)H;

        gluPerspective(52.0f, (GLfloat)W/(GLfloat)H, 1.0f, 1000.0f);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_TEXTURE_2D);
        m_Tex1 = new CTargaImage;

        glGenTextures(5, m_ObjTex1);
        if(!m_Tex1->Load("wall_512_5_05.tga")) return;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[0]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_Tex1->GetWidth(), m_Tex1->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, m_Tex1->GetImage());

        if(!m_Tex1->Load("granite_slabs_Q3_2.tga")) return;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[1]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_Tex1->GetWidth(), m_Tex1->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, m_Tex1->GetImage());

        if(!m_Tex1->Load("brick3.tga")) return;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[2]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_Tex1->GetWidth(), m_Tex1->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, m_Tex1->GetImage());

        if(!m_Tex1->Load("granite_stripes_Q3.tga")) return;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[3]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_Tex1->GetWidth(), m_Tex1->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, m_Tex1->GetImage());

        if(!m_Tex1->Load("ground_1024_v2_Q3.tga")) return;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[4]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_Tex1->GetWidth(), m_Tex1->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, m_Tex1->GetImage());
}

void cubo()
{
        glColor3f(1.0f,1.0f,1.0f);

        glBegin(GL_QUADS);

        glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.5f, -2.0f,  0.5f);
        glTexCoord2f(1.0f, 0.0f); glVertex3f( 0.5f, -2.0f,  0.5f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f( 0.5f,  2.0f,  0.5f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.5f,  2.0f,  0.5f);

        glTexCoord2f(1.0f, 0.0f); glVertex3f(-0.5f, -2.0f, -0.5f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f(-0.5f,  2.0f, -0.5f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f( 0.5f,  2.0f, -0.5f);
        glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.5f, -2.0f, -0.5f);

        glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.5f,  2.0f, -0.5f);
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.5f,  2.0f,  0.5f);
        glTexCoord2f(1.0f, 0.0f); glVertex3f( 0.5f,  2.0f,  0.5f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f( 0.5f,  2.0f, -0.5f);

        glTexCoord2f(1.0f, 1.0f); glVertex3f(-0.5f, -2.0f, -0.5f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f( 0.5f, -2.0f, -0.5f);
        glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.5f, -2.0f,  0.5f);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(-0.5f, -2.0f,  0.5f);

        glTexCoord2f(1.0f, 0.0f); glVertex3f( 0.5f, -2.0f, -0.5f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f( 0.5f,  2.0f, -0.5f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f( 0.5f,  2.0f,  0.5f);
        glTexCoord2f(0.0f, 0.0f); glVertex3f( 0.5f, -2.0f,  0.5f);

        glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.5f, -2.0f, -0.5f);
        glTexCoord2f(1.0f, 0.0f); glVertex3f(-0.5f, -2.0f,  0.5f);
        glTexCoord2f(1.0f, 1.0f); glVertex3f(-0.5f,  2.0f,  0.5f);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.5f,  2.0f, -0.5f);

        glEnd();
}

void Idle(void)
{
        glutPostRedisplay();
}

void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        glPushMatrix();
        glTranslatef(1.5,-1.0, -8.0);
        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[0]);
        cubo();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-1.5,-1.0, -8.0);
        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[1]);
        cubo();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(1.5,-1.0, -2.0);
        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[2]);
        cubo();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-1.5,-1.0, -2.0);
        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[3]);
        cubo();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.0, 0.0, -3.0);

        glBindTexture(GL_TEXTURE_2D, m_ObjTex1[4]);

        glBegin(GL_TRIANGLE_STRIP);
        glTexCoord2f(x2, x2); glVertex3f(-1.0, -1.0, -1.0);
        glTexCoord2f(equis, x2); glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2f(x2, y2); glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2f(equis, y2); glVertex3f(1.0, -1.0, 1.0);
        glEnd();
        glPopMatrix();

        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                equis = 0.;
                ye = 0.0;
                break;

        case 's':
        case 'S':
                equis = 1.0;
                ye = 1.0;
                break;

        case 'd':
        case 'D':
                equis = 0.2;
                ye = 0.2;
                break;
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                //que ahce
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:

                        break;
                case GLUT_RIGHT_BUTTON:

                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(W, H);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        //Liberar memoria:
        glDeleteTextures(5, &m_ObjTex1[0]);
        glDeleteTextures(5, &m_ObjTex1[1]);
        glDeleteTextures(5, &m_ObjTex1[2]);
        glDeleteTextures(5, &m_ObjTex1[3]);
        glDeleteTextures(5, &m_ObjTex1[4]);
        m_Tex1->Release();
        delete m_Tex1;

        return 0;
}

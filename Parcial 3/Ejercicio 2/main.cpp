#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

void inicializar(void);
using namespace std;
int W=800, H=600;
float aspect = (float)W/(float)H;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;

float cameraAngleX = 0;
float cameraAngleY = 0;



GLUquadricObj *m_Sphere;


void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        aspect = (float)W/(float)H;

        gluPerspective(90.0,(GLfloat)W/(GLfloat)H, 1.0, 100.0);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST);

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        m_Sphere = gluNewQuadric();

        TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
        glutPostRedisplay();
}

GLubyte Figura[]={
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x03, 0x80, 0x01, 0xc0, 0x06, 0xc0, 0x03, 0x60,
        0x04, 0x60, 0x06, 0x20, 0x04, 0x30, 0x0C, 0x20,
        0x04, 0x18, 0x18, 0x20, 0x04, 0x0c, 0x30, 0x20,
        0x04, 0x18, 0x18, 0x20, 0x44, 0x03, 0xc0, 0x22,
        0x44, 0x01, 0x80, 0x22, 0x44, 0x01, 0x80, 0x22,
        0x44, 0x01, 0x80, 0x11, 0x44, 0x01, 0x80, 0x22,
        0x44, 0x01, 0x80, 0x11, 0x44, 0x01, 0x80, 0x22,
        0x66, 0x01, 0x80, 0x66, 0x33, 0x01, 0x80, 0xcc,
        0x19, 0x81, 0x81, 0x98, 0x0c, 0xc1, 0x83, 0x30,
        0x07, 0xe1, 0x87, 0xe0, 0x03, 0x3f, 0xfc, 0xc0,
        0x03, 0x31, 0x8c, 0xc0, 0x0c, 0x33, 0xcc, 0xc0,
        0x06, 0x64, 0x26, 0x60, 0x0c, 0xcc, 0x33, 0x30,
        0x18, 0xcc, 0x33, 0x18, 0x10, 0x30, 0x0c, 0x08,
        0x10, 0x63, 0xc6, 0x08, 0x10, 0x30, 0x0c, 0x08,
        0x10, 0x18, 0x18, 0x08, 0x10, 0x00, 0x00, 0x08,
};

GLubyte antorcha[] {
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
        0x00,0x00,0x00,0xC0,0x00,0x00,0x01,0xF0,
        0x0F,0x00,0x07,0xF0,0x1F,0x80,0x0F,0xE0,
        0x0F,0xC0,0x0F,0xC0,0x07,0xE0,0x7F,0x80,
        0x03,0xF0,0xFE,0x00,0x03,0xF4,0xFF,0x80,
        0x07,0xFC,0xFF,0xE0,0x0F,0xFC,0xBF,0xF8,
        0xFF,0xE3,0xB7,0xE8,0xDE,0x80,0xBB,0x70,
        0x71,0x10,0x4F,0x00,0x03,0x10,0x4E,0x40,
        0x02,0x88,0x8C,0x20,0x05,0x05,0x04,0x40,
        0x02,0x82,0x14,0x40,0x02,0x40,0x10,0x80,
        0x02,0x64,0x1A,0x80,0x00,0x92,0x29,0x00,
        0x00,0xB0,0x48,0x00,0x00,0xC8,0x90,0x00,
        0x00,0x85,0x10,0x00,0x00,0x03,0x00,0x00,
        0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00
};

void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
        gluLookAt(0.0,1.0,6.0,0.0,0.0,0.0,0.0,1.0,0.0);

        glEnable(GL_POLYGON_STIPPLE);
        glPolygonStipple(antorcha);
        glBegin(GL_TRIANGLES);
        glColor3f(1.0,0.0,0.0);
        glVertex3f(2.0,2.5,-1.0);
        glColor3f(0.0,1.0,0.0);
        glVertex3f(-3.5, -2.5,-1.0);
        glColor3f(0.0,0.0,1.0);
        glVertex3f(2.0,-4.0,0.0);
        glEnd();
        glDisable(GL_POLYGON_STIPPLE);

        glEnable(GL_POLYGON_STIPPLE);
        glPolygonStipple(Figura);
        glBegin(GL_POLYGON);
        glColor3f(1.0,0.0,0.0);
        glVertex3f(-1.0,2.0,0.0);
        glColor3f(0.0,1.0,0.0);
        glVertex3f(-3.0,-0.5,0.0);
        glColor3f(0.0,0.0, 1.0);
        glVertex3f(1.5,-3.0,0.0);
        glColor3f(1.0,1.0,0.0);
        glVertex3f(1.0,-2.0,0.0);
        glColor3f(1.0,0.0,1.0);
        glVertex3f(1.0,1.0,0.0);
        glEnd();
        glDisable(GL_POLYGON_STIPPLE);
        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'w':
        case 'W': cameraAngleX--; break;

        case 's':
        case 'S': cameraAngleX++; break;

        case 'a':
        case 'A': cameraAngleY--; break;

        case 'd':
        case 'D': cameraAngleY++; break;

                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                //que ahce
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:

                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        //Free memory
        if(m_Sphere) gluDeleteQuadric(m_Sphere);
        m_Sphere = NULL;

        return 0;
}

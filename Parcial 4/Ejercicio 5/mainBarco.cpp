#include <iostream>
#include <stdio.h>
#include <time.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL
#include <math.h>
#include "CTargaImage.h"

void inicializar(void);
using namespace std;
int W=800, H=600;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;
float C = 1000;

float clipAreaXLeft = 0;
float clipAreaXRight = 0;
float clipAreaYBottom = 0;
float clipAreaYTop = 0;

float ex = 0;
float ey = 1.0f;
float ez = 3.0f;

bool a = false;
bool s = false;
bool w = false;
bool d = false;

int i = 0;

unsigned int lista;

float PI2 = 3.14159265359 * 2;

void NewAspect(){
        float aspect = (float)W / (float)H;
        if(W >= H) {
                clipAreaXLeft = -1.0 * aspect;
                clipAreaXRight = 1.0 * aspect;
                clipAreaYBottom = -1.0;
                clipAreaYTop = 1.0;
        }
        else{
                clipAreaXLeft = -1.0f;
                clipAreaXRight = 1.0f;
                clipAreaYBottom = -1.0 / aspect;
                clipAreaYTop = 1.0 / aspect;
        }

        gluOrtho2D(clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop);
}

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(52.0f, (GLfloat)W/(GLfloat)H, 1.0f, 100.0f);
        //NewAspect();
        //glOrtho(0, W-1, 0, H-1, -1, 1);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST); //Profundidad

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
        TimeStart = glutGet(GLUT_ELAPSED_TIME);

        C -= deltaTime;
        glutPostRedisplay();
}

void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        gluLookAt(ex, ey, ez, 0, 0, 0, 0, 1.0f, 0);

        glListBase(0);
        lista = glGenLists(1);
        glNewList(lista, GL_COMPILE);
        glPolygonMode(GL_FRONT, GL_FILL);
        int caras = 5;
        float radio = 0.15f;
        float x_aux = 0.0f;
        float y_aux = 0.0f;
        float z_aux = 0.0f;
        float inc = PI2/(float)caras;
        float inc2 = PI2/(float)30;

        glPushMatrix();
        glBegin(GL_QUADS);
        glColor3f(0.6f, 0.4f, 0.2f);
        glVertex2f(-1.0f, -0.8f);         ///bottom left
        glVertex2f(1.0f, -0.8);         ///bottom right
        glVertex2f(1.5f, -0.2f);         ///top right
        glVertex2f(-1.5f, -0.2f);         ///top left
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glBegin(GL_QUADS);
        glColor3f(0.5f, 0.5f, 0.5f);
        glVertex2f(-0.2f, -0.2f);         ///bottom left
        glVertex2f(1.0f, -0.2f);         ///bottom right
        glVertex2f(1.0f, 0.4f);         ///top right
        glVertex2f(-0.2f, 0.4f);         ///top left
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.1f, 0.1f,0);
        glBegin(GL_POLYGON);
        for(float angulo = PI2; angulo > 0.0f; angulo -= inc)
        {
                x_aux = float(radio * sin(angulo));
                y_aux = float(radio * cos(angulo));
                glColor3f(1.0f, 1.0f, 0);
                glVertex3f(x_aux, y_aux, 0.1f);
        }
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.6f, 0.1f,0);
        glBegin(GL_POLYGON);
        for(float angulo = PI2; angulo > 0.0f; angulo -= inc)
        {
                x_aux = float(radio * sin(angulo));
                y_aux = float(radio * cos(angulo));
                glColor3f(1.0f, 1.0f, 0);
                glVertex3f(x_aux, y_aux, 0.1f);
        }
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.095f, 0.13f,0);
        glBegin(GL_POLYGON);
        for(float angulo = PI2; angulo > 0.0f; angulo -= inc)
        {
                x_aux = float(0.13f * sin(angulo));
                y_aux = float(0.13f * cos(angulo));
                glColor3f(1.0f, 1.0f, 1.0f);
                glVertex3f(x_aux, y_aux, 0.2f);
        }
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.58f, 0.13f,0);
        glBegin(GL_POLYGON);
        for(float angulo = PI2; angulo > 0.0f; angulo -= inc)
        {
                x_aux = float(0.13f * sin(angulo));
                y_aux = float(0.13f * cos(angulo));
                glColor3f(1.0f, 1.0f, 1.0f);
                glVertex3f(x_aux, y_aux, 0.2f);
        }
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.3f, 0.6f, 0);
        glBegin(GL_QUADS);
        glColor3f(1.0f, 0, 0);
        glVertex2f(-0.3f, -0.2f);         ///bottom left
        glVertex2f(0.3f, -0.2f);         ///bottom right
        glVertex2f(0.3f, 0.2f);         ///top right
        glVertex2f(-0.3f, 0.2f);         ///top left
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.2f, 0.64f, 0);
        glBegin(GL_POLYGON);
        for(float angulo = PI2; angulo > 0.0f; angulo -= inc2)
        {
                x_aux = float(0.1f * sin(angulo));
                y_aux = float(0.1f * cos(angulo));
                glColor3f(1.0f, 1.0f, 1.0f);
                glVertex3f(x_aux, y_aux, 0.2f);
        }
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.2f, 0.64f, 0);
        glBegin(GL_POLYGON);
        for(float angulo = PI2; angulo > 0.0f; angulo -= inc2)
        {
                x_aux = float(0.12f * sin(angulo));
                y_aux = float(0.12f * cos(angulo));
                glColor3f(1.0f, 0.5f, 0);
                glVertex3f(x_aux, y_aux, 0.18f);
        }
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.3f, 0.6f, 0);
        glBegin(GL_QUADS);
        glColor3f(0, 1.0f, 0);
        glVertex2f(-0.33f, -0.2f);         ///bottom left
        glVertex2f(0.34f, -0.2f);         ///bottom right
        glVertex2f(0.34f, 0.22f);         ///top right
        glVertex2f(-0.33f, 0.22f);         ///top left
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-1.2f, -0.2f, 0);
        glBegin(GL_QUADS);
        glColor3f(0.6f, 0.4f, 0.2f);
        glVertex2f(0, 0);         ///bottom left
        glVertex2f(0.05, 0);         ///bottom right
        glVertex2f(0.05, 0.5);         ///top right
        glVertex2f(0, 0.5);         ///top left
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-1.2f, 0.3f, 0);
        glBegin(GL_TRIANGLES);
        glColor3f(0.6f, 0.4f, 0.2f);
        glVertex2f(0, 0);
        glVertex2f(0, 0.4);
        glVertex2f(0.4, 0);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-1.2f, 0.3f, 0.1f);
        glBegin(GL_TRIANGLES);
        glColor3f(0, 1.0f, 0);
        glVertex2f(0.07f, 0.05f);
        glVertex2f(0.07f, 0.35f);
        glVertex2f(0.37f, 0.05f);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.3f, -0.6f, 0.05f);
        glBegin(GL_QUADS);
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex2f(0, 0);         ///bottom left
        glVertex2f(0.2f, 0);         ///bottom right
        glVertex2f(0.2f, 0.2f);         ///top right
        glVertex2f(0, 0.2f);         ///top left
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.3f, -0.6f, 0.05f);
        glBegin(GL_QUADS);
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex2f(0, 0);         ///bottom left
        glVertex2f(0.2f, 0);         ///bottom right
        glVertex2f(0.2f, 0.2f);         ///top right
        glVertex2f(0, 0.2f);         ///top left
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-0.6f, -0.6f, 0.05f);
        glBegin(GL_QUADS);
        glColor3f(1.0f, 1.0f, 1.0f);
        glVertex2f(0, 0);         ///bottom left
        glVertex2f(0.2f, 0);         ///bottom right
        glVertex2f(0.2f, 0.2f);         ///top right
        glVertex2f(0, 0.2f);         ///top left
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-0.6f, -0.73f, 0.05f);
        glBegin(GL_QUADS);
        glColor3f(0, 0, 1.0f);
        glVertex2f(-1.0f, 0);         ///bottom left
        glVertex2f(2.0f, 0);         ///bottom right
        glVertex2f(2.0f, 0.05f);         ///top right
        glVertex2f(-1.0f, 0.05f);         ///top left
        glEnd();
        glPopMatrix();

        glEndList();

        glCallList(lista);

        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                a = true;
                break;

        case 's':
        case 'S':
                s = true;
                break;

        case 'w':
        case 'W':
                w = true;
                break;

        case 'd':
        case 'D':
                d = true;
                break;

        case 'q':
        case 'Q':
                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                a = false;
                break;

        case 's':
        case 'S':
                s = false;
                break;

        case 'w':
        case 'W':
                w = false;
                break;

        case 'd':
        case 'D':
                d = false;
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:
                        std::cout << x << "  "<< y << std::endl;
                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        glDeleteLists(lista, 1);

        return 0;
}

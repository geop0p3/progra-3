#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

void inicializar(void);
using namespace std;
int W=800, H=600;
float aspect = (float)W/(float)H;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;


float cameraAngleX = 0;
float cameraAngleY = 0;

float clipAreaL,
      clipAreaR,
      clipAreaT,
      clipAreaB;

int caras = 5;
float pi = 3.14159265359 * 2;
float x = 0.0f;
float y = 0.0f;
float inc = pi/(float)caras;
float inc2 = pi/(float) 30;

unsigned int lis;


void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        aspect = (float)W/(float)H;

        if(W >= H)
        {
                clipAreaL = -1.0 * aspect;
                clipAreaR = 1.0 * aspect;
                clipAreaB = -1.0;
                clipAreaT = 1.0;
        }
        else
        {
                clipAreaL = -1.0;
                clipAreaR = 1.0;
                clipAreaB = -1.0 / aspect;
                clipAreaT = 1.0 / aspect;
        }
        gluOrtho2D(clipAreaL, clipAreaR, clipAreaB, clipAreaT);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST); //Profundidad

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
        glutPostRedisplay();
}


void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        glListBase(0);
        lis = glGenLists(1);
        glNewList(lis, GL_COMPILE);
        glPolygonMode(GL_FRONT, GL_FILL);

        glPushMatrix();
        glBegin(GL_QUADS);
        glColor3f(0.6f, 0.4f, 0.2f);
        glVertex2f(-0.7f, -0.7f);
        glVertex2f(0.7f, -0.7);
        glVertex2f(1.2f, -0.2f);
        glVertex2f(-1.2f, -0.2f);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-0.3f, -0.1,0);
        glBegin(GL_QUADS);
        glColor3f(0.3f, 0.3f, 0.3f);
        glVertex2f(-0.2f, -0.2f);
        glVertex2f(1.0f, -0.2f);
        glVertex2f(1.0f, 0.3f);
        glVertex2f(-0.2f, 0.3f);
        glEnd();
        glPopMatrix();


        glPushMatrix();
        glTranslatef(-0.2f, 0.0f,0);
        glBegin(GL_POLYGON);
        for(float angulo = pi; angulo > 0.0f; angulo -= inc)
        {
                x = float(0.13f * sin(angulo));
                y = float(0.13f * cos(angulo));
                glColor3f(1.0f, 1.0f, 1.0f);
                glVertex3f(x, y, 0.2f);
        }
        glEnd();
        glPopMatrix();



        glPushMatrix();
        glTranslatef(0.4f, 0.0,0);
        glBegin(GL_POLYGON);
        for(float angulo = pi; angulo > 0.0f; angulo -= inc)
        {
                x = float(0.13f * sin(angulo));
                y = float(0.13f * cos(angulo));
                glColor3f(1.0f, 1.0f, 1.0f);
                glVertex3f(x, y, 0.2f);
        }
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-1.0f, 0.3f, 0);
        glBegin(GL_TRIANGLES);
        glColor3f(1.0, 0.0f, 0.8f);
        glVertex2f(0, 0);
        glVertex2f(0, 0.2);
        glVertex2f(0.4, 0);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-1.0f, -0.2f, 0);
        glBegin(GL_QUADS);
        glColor3f(0.6f, 0.4f, 1.0f);
        glVertex2f(0, 0);
        glVertex2f(0.05, 0);
        glVertex2f(0.05, 0.5);
        glVertex2f(0, 0.5);
        glEnd();
        glPopMatrix();


        glPushMatrix();
        glTranslatef(-0.6f, -1.68f, 0.05f);
        glBegin(GL_QUADS);
        glColor3f(0, 0.4, 1.0f);
        glVertex2f(-1.0f, 0);
        glVertex2f(2.0f, 0);
        glVertex2f(2.0f, 1.0f);
        glVertex2f(-1.0f, 1.0f);
        glEnd();
        glPopMatrix();

        // ciruclo
        glPushMatrix();
        glTranslatef(0.7f, -0.4f, 0);
        glBegin(GL_POLYGON);
        for(float angulo = pi; angulo > 0.0f; angulo -= inc2)
        {
                x = float(0.12f * sin(angulo));
                y = float(0.12f * cos(angulo));
                glColor3f(1.0, 1.0, 1.0);
                glVertex3f(x, y, 0.18f);
        }
        glEnd();
        glPopMatrix();

        // ciruclo
        glPushMatrix();
        glTranslatef(0.3f, -0.4f, 0);
        glBegin(GL_POLYGON);
        for(float angulo = pi; angulo > 0.0f; angulo -= inc2)
        {
                x = float(0.12f * sin(angulo));
                y = float(0.12f * cos(angulo));
                glColor3f(1.0, 1.0, 1.0);
                glVertex3f(x, y, 0.18f);
        }
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.1f, 0.1f, 0);
        glBegin(GL_QUADS);
        glColor3f(1.0, 1.0f, 0);
        glVertex2f(-0.5f, -0.2f);
        glVertex2f(0.5f, -0.2f);
        glVertex2f(0.5f, 0.3);
        glVertex2f(-0.5f, 0.3);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-0.2, 0.2f, 0);
        glBegin(GL_QUADS);
        glColor3f(1.0, 1.0f, 1.0);
        glVertex2f(-0.1f, -0.2f);
        glVertex2f(0.1f, -0.2f);
        glVertex2f(0.1f, 0.3);
        glVertex2f(-0.1f, 0.3);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(-0.2f, 0.23f, 0);
        glBegin(GL_QUADS);
        glColor3f(1.0, 0.2f, 0.2);
        glVertex2f(-0.1f, -0.2f);
        glVertex2f(0.1f, -0.2f);
        glVertex2f(0.1f, 0.3);
        glVertex2f(-0.1f, 0.3);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.1f, 0.3f, 0);
        glBegin(GL_QUADS);
        glColor3f(1.0, 1.0f, 1.0);
        glVertex2f(-0.1f, -0.2f);
        glVertex2f(0.1f, -0.2f);
        glVertex2f(0.1f, 0.3);
        glVertex2f(-0.1f, 0.3);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.1f, 0.33f, 0);
        glBegin(GL_QUADS);
        glColor3f(1.0, 0.2f, 0.2);
        glVertex2f(-0.1f, -0.2f);
        glVertex2f(0.1f, -0.2f);
        glVertex2f(0.1f, 0.3);
        glVertex2f(-0.1f, 0.3);
        glEnd();
        glPopMatrix();


        glPushMatrix();
        glTranslatef(0.4f, 0.4f, 0);
        glBegin(GL_QUADS);
        glColor3f(1.0, 1.0f, 1.0);
        glVertex2f(-0.1f, -0.2f);
        glVertex2f(0.1f, -0.2f);
        glVertex2f(0.1f, 0.3);
        glVertex2f(-0.1f, 0.3);
        glEnd();
        glPopMatrix();

        glPushMatrix();
        glTranslatef(0.4f, 0.43f, 0);
        glBegin(GL_QUADS);
        glColor3f(1.0, 0.2f, 0.2);
        glVertex2f(-0.1f, -0.2f);
        glVertex2f(0.1f, -0.2f);
        glVertex2f(0.1f, 0.3);
        glVertex2f(-0.1f, 0.3);
        glEnd();
        glPopMatrix();

        // ciruclo
        glPushMatrix();
        glTranslatef(-0.2f, -0.4f, 0);
        glBegin(GL_POLYGON);
        for(float angulo = pi; angulo > 0.0f; angulo -= inc2)
        {
                x = float(0.12f * sin(angulo));
                y = float(0.12f * cos(angulo));
                glColor3f(1.0, 1.0, 1.0);
                glVertex3f(x, y, 0.18f);
        }
        glEnd();
        glPopMatrix();

        // ciruclo
        glPushMatrix();
        glTranslatef(-0.6f, -0.4f, 0);
        glBegin(GL_POLYGON);
        for(float angulo = pi; angulo > 0.0f; angulo -= inc2)
        {
                x = float(0.12f * sin(angulo));
                y = float(0.12f * cos(angulo));
                glColor3f(1.0, 1.0, 1.0);
                glVertex3f(x, y, 0.18f);
        }
        glEnd();
        glPopMatrix();

        glEndList();

        glCallList(lis);

        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'w':
        case 'W': cameraAngleX--; break;

        case 's':
        case 'S': cameraAngleX++; break;

        case 'a':
        case 'A': cameraAngleY--; break;

        case 'd':
        case 'D': cameraAngleY++; break;

                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                //que ahce
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:

                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar


        return 0;
}


#include <iostream>
#include <stdio.h>
#include <time.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL
#include <math.h>
#include "Obj.cpp"
#include "Obj.h"
#include "Utilidad.cpp"
#include "Utilidad.h"
#define TERRENO 65

void inicializar(void);
using namespace std;
int W=800, H=600;

unsigned int g_Textre[100] = {0};
CLoadOBJ g_LoadObj;
t3DModel g_3DModel;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;
float C = 1000;

float RotateX = 0.0;
float VelocidadRotacion = 0.0;

float clipAreaXLeft = 0;
float clipAreaXRight = 0;
float clipAreaYBottom = 0;
float clipAreaYTop = 0;

int i = 0;

int m_windowWidth;
int m_windowHeight;
GLubyte mapa_terreno[TERRENO * TERRENO];

void NewAspect(){
        float aspect = (float)W / (float)H;
        if(W >= H) {
                clipAreaXLeft = -1.0 * aspect;
                clipAreaXRight = 1.0 * aspect;
                clipAreaYBottom = -1.0;
                clipAreaYTop = 1.0;
        }
        else{
                clipAreaXLeft = -1.0f;
                clipAreaXRight = 1.0f;
                clipAreaYBottom = -1.0 / aspect;
                clipAreaYTop = 1.0 / aspect;
        }

        gluOrtho2D(clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop);
}

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(52.0f, (GLfloat)W/(GLfloat)H, 1.0f, 100.0f);
        //NewAspect();
        //glOrtho(0, W-1, 0, H-1, -1, 1);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST); //Profundidad
        gl_LoadObj.ImportObj(&g_3DModel, "foot.obj");
        gl_LoadObj.AddMaterial(&g_3Dmodel, "Bone", "Bone.bmp", 255,255,255);
        gl_LoadObj.SetObjectMaterial(&g_3DModel, 0, 0);


        for (size_t i = 0; i < g_3DModel.numOfMaterials; i++) {
                if (strlen(g_3DModel.pMaterials[i].strFile)>0) {
                        CreateTexture(g_Texture[i], g_3Dmodel.pMaterials[i].strFile);
                }
                g_3DModel.pMaterials[i].textureId = i;
        }

        glEnable(GL_LIGHTNING);
        glEnable(GL_LIGHT0);
        glEnable(GL_COLOR_MATERIAL);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        FILE *pFILE = fopen("1.raw", "rb");
        if(!pFILE)
                exit(0);

        fread(&mapa_terreno, TERRENO * TERRENO, 1, pFILE);
        fclose(pFILE);

        TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
        TimeStart = glutGet(GLUT_ELAPSED_TIME);

        C -= deltaTime;
        glutPostRedisplay();
}

void DibujarTerreno(){
        for (int z=0; z<TERRENO; z++) {
                glBegin(GL_TRIANGLE_STRIP);
                for(int x=0; x<TERRENO; x++) {
                        float y1 = mapa_terreno[z*TERRENO + x]/25.0f;
                        float y2 = mapa_terreno[(z+1)*TERRENO+x]/25.0f;

                        glColor3f(0.1f,0.5f + 0.5f*y1/25.0f, 0.1f);
                        glVertex3f((x-TERRENO/2), y1, (z-TERRENO/2));
                        glColor3f(0.1f, 0.5f + 0.5f*y2/25.0f, 0.1f);
                        glVertex3f((x-TERRENO/2), y2, (z+1-TERRENO/2));
                }
                glEnd();
        }
}

void DibujarAgua(){
        glPushMatrix();
        glColor3f(0.0f, 0.0f, 1.0f);
        glBegin(GL_QUADS);
        glVertex3f(-33.0f, 3.0f, 33.0f);
        glVertex3f(33.0f, 3.0f, 33.0f);
        glVertex3f(33.0f, 3.0f, -33.0f);
        glVertex3f(-33.0f, 3.0f, -33.0f);
        glEnd();
        glPopMatrix();
}

void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        gluLookAt(TERRENO / 3.0f, 56.0f, TERRENO / 3.0f, 0, 0, 0, 0, 1.0f, 0);

        DibujarTerreno();
        DibujarAgua();

        if(a) {
                ex+=0.1f;
        }
        if(s) {
                ez+=0.1f;
        }
        if(w) {
                ez-=0.1f;
        }
        if(d) {
                ex-=0.1f;
        }


        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                a = true;
                break;

        case 's':
        case 'S':
                s = true;
                break;

        case 'w':
        case 'W':
                w = true;
                break;

        case 'd':
        case 'D':
                d = true;
                break;

        case 'q':
        case 'Q':
                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                a = false;
                break;

        case 's':
        case 'S':
                s = false;
                break;

        case 'w':
        case 'W':
                w = false;
                break;

        case 'd':
        case 'D':
                d = false;
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:
                        std::cout << x << "  "<< y << std::endl;
                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        return 0;
}

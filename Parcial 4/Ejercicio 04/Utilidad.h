#ifndef UTILIDAD_H
#define UTILIDAD_H

#include <windef.h> ///BYTE
#include <vector>

using std::vector;

///Vector 3
class CVector3
{
public:
	float x, y, z;
};

///Vector2 usado para UV
class CVector2
{
public:
	float x, y;
};

///Esta estructura ayuda saber que indice de los vertices y texturas usara, lo que se conoce como poligonos
struct tFace
{
	int vertIndex[3];			// indicies for the verts that make up this triangle
	int coordIndex[3];			// indicies for the tex coords to texture this face
};

///Contenedor de información del material
struct tMaterialInfo
{
	char  strName[255];			// The texture name
	char  strFile[255];			// The texture file name (If this is set it's a texture map)
	BYTE  color[3];				// The color of the object (R, G, B)
	int   texureId;				// the texture ID
	float uTile;				// u tiling of texture  (Currently not used)
	float vTile;				// v tiling of texture	(Currently not used)
	float uOffset;			    // u offset of texture	(Currently not used)
	float vOffset;				// v offset of texture	(Currently not used)
} ;

///Contenedor de objecto 3d
struct t3DObject
{
	int  numOfVerts;			// The number of verts in the model
	int  numOfFaces;			// The number of faces in the model
	int  numTexVertex;			// The number of texture coordinates
	int  materialID;			// The texture ID to use, which is the index into our texture array
	bool bHasTexture;			// This is TRUE if there is a texture map for this object
	char strName[255];			// The name of the object
	CVector3  *pVerts;			// The object's vertices
	CVector3  *pNormals;		// The object's normals
	CVector2  *pTexVerts;		// The texture's UV coordinates
	tFace *pFaces;				// The faces information of the object
};

///Contenedor de modelos
struct t3DModel
{
	int numOfObjects;					// The number of objects in the model
	int numOfMaterials;					// The number of materials for the model
	vector<tMaterialInfo> pMaterials;	// The list of material information (Textures and colors)
	vector<t3DObject> pObject;			// The object list for our model
};

bool CreateTexture(unsigned int &textureID, LPTSTR szFileName);

class Utilidad
{
    public:
        Utilidad();

    protected:

    private:
};

#endif // UTILIDAD_H

#include "Utilidad.h"
#include <windows.h>
#include <gl/gl.h>

#define GL_BGR_EXT 0x80E0 ///Lo hacemos compatbiel con bitmaps de windows

bool CreateTexture(unsigned int &textureID, LPTSTR szFileName)
{
	HBITMAP hBMP;///Contenedor de Bitmap
	BITMAP  bitmap;

	glGenTextures(1, &textureID);///Creamos 1 textura
	///Cargamos bitmap
	hBMP=(HBITMAP)LoadImage(GetModuleHandle(NULL), szFileName, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION | LR_LOADFROMFILE );

	if (!hBMP)///Se pudo cargar?
		  return FALSE;

    ///Obtenemos objecto
    ///Imaen, tama�o, y buffer para obtener infomracion
	GetObject(hBMP, sizeof(bitmap), &bitmap);
	///Se usara 4 bytes para el color
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	///Creamos textura
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, bitmap.bmWidth, bitmap.bmHeight, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, bitmap.bmBits);
	///Liberamos
	DeleteObject(hBMP);

	return TRUE;
}

Utilidad::Utilidad()
{
    //ctor
}


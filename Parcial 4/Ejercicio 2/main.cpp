#include <iostream>
#include <stdio.h>
#include <time.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL
#include <math.h>

void inicializar(void);
using namespace std;
int W=800, H=600;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;
float C = 1000;

float clipAreaXLeft = 0;
float clipAreaXRight = 0;
float clipAreaYBottom = 0;
float clipAreaYTop = 0;

float ex = 0;
float ey = 1.0f;
float ez = 3.0f;

bool a = false;
bool s = false;
bool w = false;
bool d = false;

int i = 0;

unsigned int filter;
unsigned int fogMode[]={GL_EXP, GL_EXP2, GL_LINEAR};
unsigned int fogfilter = 0;
float fogColor[4]={0.5f, 0.5f, 0.5f, 1.0f};
float pos;
bool Avanzar = true;

void NewAspect(){
        float aspect = (float)W / (float)H;
        if(W >= H) {
                clipAreaXLeft = -1.0 * aspect;
                clipAreaXRight = 1.0 * aspect;
                clipAreaYBottom = -1.0;
                clipAreaYTop = 1.0;
        }
        else{
                clipAreaXLeft = -1.0f;
                clipAreaXRight = 1.0f;
                clipAreaYBottom = -1.0 / aspect;
                clipAreaYTop = 1.0 / aspect;
        }

        gluOrtho2D(clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop);
}

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(52.0f, (GLfloat)W/(GLfloat)H, 1.0f, 1000.0f);
        //NewAspect();
        //glOrtho(0, W-1, 0, H-1, -1, 1);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST); //Profundidad

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

        glClearDepth(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

        glFogi(GL_FOG_MODE, fogMode[fogfilter]);
        glFogfv(GL_FOG_COLOR, fogColor);
        glFogf(GL_FOG_DENSITY, 0.35f);
        glHint(GL_FOG_HINT, GL_DONT_CARE);
        glFogf(GL_FOG_START, 4.0f);
        glFogf(GL_FOG_END, 15.0f);
        glEnable(GL_FOG);

        pos = 0.0f;

        TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
        TimeStart = glutGet(GLUT_ELAPSED_TIME);

        if(Avanzar) {
                pos += 5.0f * 0.01f;
                if(pos >= 5.0f)
                {
                        pos = 5.0f;
                        Avanzar = false;
                }
        }
        else{
                pos -= 5.0f * 0.01f;
                if(pos <= -5.0f)
                {
                        pos = -5.0f;
                        Avanzar = true;
                }
        }

        glutPostRedisplay();
}

void DibujarCuadro(){
        for(int x=0; x<3; x++) {
                glBegin(GL_QUAD_STRIP);
                for(int y=0; y<4; y++) {
                        glVertex3f(x,y, 0.0f);
                        glVertex3f(x+1.0f, y, 0.0f);
                }
                glEnd();
        }
}

void GPressed(){
        fogfilter+=1;
        if(fogfilter>2) {
                fogfilter = 0;
        }
        glFogi(GL_FOG_MODE, fogMode[fogfilter]);
}

void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        gluLookAt(5.0f, 7.0f, 5.0f, 1.5f, 1.5f, 0, 0, 1.0f, 0);

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        glTranslatef(pos, 0.0f, 0.0f);
        glPushMatrix();
        glColor3f(1.0f, 1.0f, 1.0f);
        DibujarCuadro();
        glColor3f(1.0f, 0.0f, 1.0f);
        glTranslatef(3.0f, 3.0f, -3.0f);
        glRotatef(180.0f, 1.0f, 0.0f, 1.0f);
        DibujarCuadro();
        glColor3f(1.0f, 1.0f, 0.0f);
        glTranslatef(0.0f, 3.0f, 0.0f);
        glRotatef(180.0f, 1.0f, 0.0f, 1.0f);
        glRotatef(180.0f, 0.0f, -1.0f, 0.0f);
        DibujarCuadro();
        glColor3f(0.0f, 1.0f, 1.0f);
        glTranslatef(3.0f, 3.0f, -3.0f);
        glRotatef(180.0f, 1.0f, 0.0f, 1.0f);
        DibujarCuadro();
        glColor3f(0.0f, 0.0f, 0.6f);
        glTranslatef(0.0f, 0.0f, -3.0f);
        glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
        glRotatef(180.0f, 0.0f, 1.0f, 1.0f);
        DibujarCuadro();
        glColor3f(1.0f, 0.0f, 0.0f);
        glTranslatef(3.0f, 0.0f, -3.0f);
        glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
        DibujarCuadro();
        glPopMatrix();

        //rayas
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glPushMatrix();
        glColor3f(0.0f, 0.0f, 0.0f);
        DibujarCuadro();
        glTranslatef(3.0f, 3.0f, -3.0f);
        glRotatef(180.0f, 1.0f, 0.0f, 1.0f);
        DibujarCuadro();
        glTranslatef(0.0f, 3.0f, 0.0f);
        glRotatef(180.0f, 1.0f, 0.0f, 1.0f);
        glRotatef(180.0f, 0.0f, -1.0f, 0.0f);
        DibujarCuadro();
        glTranslatef(3.0f, 3.0f, -3.0f);
        glRotatef(180.0f, 1.0f, 0.0f, 1.0f);
        DibujarCuadro();
        glTranslatef(0.0f, 0.0f, -3.0f);
        glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
        glRotatef(180.0f, 0.0f, 1.0f, 1.0f);
        DibujarCuadro();
        glTranslatef(3.0f, 0.0f, -3.0f);
        glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
        DibujarCuadro();
        glPopMatrix();

        glutSwapBuffers();

        deltaTime = TimeStart - TimeOld;
        TimeOld =TimeStart;
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                GPressed();
                break;

        case 's':
        case 'S':

                break;

        case 'w':
        case 'W':

                break;

        case 'd':
        case 'D':

                break;

        case 'q':
        case 'Q':
                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':

                break;

        case 's':
        case 'S':

                break;

        case 'w':
        case 'W':

                break;

        case 'd':
        case 'D':

                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:
                        std::cout << x << "  "<< y << std::endl;
                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        return 0;
}

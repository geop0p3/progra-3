#include <iostream>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL
#include "CTargaImage.h"
#include "CTargaImage.cpp"
#include <vector>
#include <stdlib.h>

void inicializar(void);
using namespace std;
int W=400, H=400;
//bool * estados_teclas = new bool [256]; //  Para poder usar más de una tecla a la vez

float Rotacion= 0.0f;

int TiempoViejo = 0;
float deltaTime= 0.0f;
int TiempoStart;
float cambiox = 0.0f;
float cambioy = 0.0f;
float tiempo = 3000.0;
float direccion = 0.004;
int contador = 0;
float jugadorx = 0.0;
float direccionx = 0.0;

float RotateX = 0.0f;
float VelocidadRotacion = 0.8f;

float clipAreaXLeft;
float clipAreaXRight;
float clipAreaYBottom;
float clipAreaYTop;
int randomsprite = 1;


float ballXmin;
float ballXmax;
float ballYmin;
float ballYmax;
float ballRadius = 0.05;

float separacion = 0.15;

CTargaImage *m_TexturaUNO;
unsigned int m_ObjetoTexturaUNO[5];

char const *imagenes[] = {"nave.tga", "enemigo.tga", "enemigo2.tga", "enemigo3.tga", "bala.tga"};

float box = 0.08;
float boxb = 0.03;

struct Enemigo
{
        bool vivo;
        bool golpe;
        int vida;
        float enemigox;
        float enemigoy;
        int sprite;

        Enemigo()
        {
                vida = 2;
                vivo = true;
                golpe = false;
                enemigox = -1.2;
                enemigoy = 0.8;
                sprite = 1;
        };
};

struct bala
{
        bool vivo;
        bool golpe;
        float balax;
        float balay;
        float velocidad;

        bala()
        {
                vivo = false;
                balax = jugadorx;
                balay = -0.8;
                velocidad = 0.0;
        };
};

struct balaenemigo
{
        bool vivo;
        bool golpe;
        float balax;
        float balay;
        float velocidad;

        balaenemigo()
        {
                vivo = true;
                balax = 0;
                balay = 0;
                velocidad = 0.0;
        };
};

Enemigo enemigos[55];
bala balax;
balaenemigo balaenemi;

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        float aspect= (float)W / (float)H;
        if (W >= H) {
                clipAreaXLeft = -1.0 * aspect;
                clipAreaXRight = 1.0 * aspect;
                clipAreaYBottom = -1.0;
                clipAreaYTop = 1.0;
        } else {
                clipAreaXLeft = -1.0;
                clipAreaXRight = 1.0;
                clipAreaYBottom = -1.0 / aspect;
                clipAreaYTop = 1.0 / aspect;
        }

        gluOrtho2D(clipAreaXLeft, clipAreaXRight,clipAreaYBottom, clipAreaYTop);
        ballXmin = clipAreaXLeft + 0.1;
        ballXmax = clipAreaXRight - 0.1;
        ballYmax = clipAreaYTop - ballRadius;

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void LoadTexture(int i){
        if(!m_TexturaUNO->Load(imagenes[i]))
                return;

        glBindTexture(GL_TEXTURE_2D, m_ObjetoTexturaUNO[i]);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_TexturaUNO->GetWidth(), m_TexturaUNO->GetHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, m_TexturaUNO->GetImage());
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);

        glEnable(GL_TEXTURE_2D);
        m_TexturaUNO = new CTargaImage;

        glGenTextures(5, m_ObjetoTexturaUNO);

        for(int i=0; i<5; i++) {
                LoadTexture(i);
        }

        for (int i=0; i<11; i++) {
                enemigos[i].enemigox += separacion;
                separacion += 0.15f;
                enemigos[i].sprite = rand() % 3 + 1;
        }

        separacion = 0.15f;

        for (int i=11; i<22; i++) {
                enemigos[i].enemigox += separacion;
                enemigos[i].enemigoy -= 0.2f;
                separacion += 0.15f;
                enemigos[i].sprite = rand() % 2 + 1;
        }
        srand (time(NULL));

        separacion = 0.15f;

        for (int i=22; i<33; i++) {
                enemigos[i].enemigox += separacion;
                enemigos[i].enemigoy -= 0.4f;
                separacion += 0.15f;
                enemigos[i].sprite = rand() % 2 + 1;
        }

        separacion = 0.15f;

        for (int i=33; i<44; i++) {
                enemigos[i].enemigox += separacion;
                enemigos[i].enemigoy -= 0.6f;
                separacion += 0.15f;
                enemigos[i].sprite = rand() % 3 + 1;
        }

        separacion = 0.15f;

        for (int i=44; i<55; i++) {
                enemigos[i].enemigox += separacion;
                enemigos[i].enemigoy -= 0.8f;
                separacion += 0.15f;
                enemigos[i].sprite = rand() % 3 + 1;
        }
        ///Encendemos luces
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);

        TiempoViejo = glutGet(GLUT_ELAPSED_TIME);    ///Simpre al final de la función

}

void Idle(void)
{
        TiempoStart = glutGet(GLUT_ELAPSED_TIME);
        tiempo -= deltaTime;
        cambiox += direccion + contador;
        jugadorx += direccionx;
        if (tiempo <= 0) {
                direccion = direccion * -1;
                tiempo = 3000;
                contador += 0.01;
                cambioy -= 0.08 + contador;
                balaenemi.velocidad = 0.0;
        }

        if (tiempo <= 0 || tiempo >= 12500) {
                balaenemi.vivo = true;
        }


        balax.velocidad += 0.02;
        balaenemi.velocidad -= 0.007;

        if (balax.balay + balax.velocidad > ballYmax + 0.01) {
                balax.vivo = false;
        }

        if (balaenemi.balay + balaenemi.velocidad < clipAreaYBottom -1.0 ) {
                balaenemi.vivo = false;
        }


        glutPostRedisplay();
}

bool colision(float _x2, float _y2, float x, float y){
        if(y + boxb < _y2 + boxb) {
                return false;
        }
        if(y > _y2 + box) {
                return false;
        }
        if(x + boxb < _x2 + boxb) {
                return false;
        }
        if(x > _x2 + box) {
                return false;
        }

        return true;
}

void enemigo()
{
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.05f, 0.05f, 0.0f); // top left
        glTexCoord2f(1.0f, 1.0f); glVertex3f(0.05f, 0.05f, 0.0f); // top right
        glTexCoord2f(1.0f, 0.0f); glVertex3f(0.05f, -0.05f, 0.0f); // bottom right
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.05f, -0.05f, 0.0f); // bottom left
        glEnd();
}

void bala()
{
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.02f, 0.02f, 0.0f); // top left
        glTexCoord2f(1.0f, 1.0f); glVertex3f(0.02f, 0.02f, 0.0f); // top right
        glTexCoord2f(1.0f, 0.0f); glVertex3f(0.02f, -0.02f, 0.0f); // bottom right
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.02f, -0.02f, 0.0f); // bottom left
        glEnd();
}

void jugador()
{
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.07f, 0.07f, 0.0f); // top left
        glTexCoord2f(1.0f, 1.0f); glVertex3f(0.07f, 0.07f, 0.0f); // top right
        glTexCoord2f(1.0f, 0.0f); glVertex3f(0.07f, -0.07f, 0.0f); // bottom right
        glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.07f, -0.07f, 0.0f); // bottom left
        glEnd();
}


void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        for (size_t i = 0; i < 55; i++) {

                glPushMatrix();
                glBindTexture(GL_TEXTURE_2D, m_ObjetoTexturaUNO[enemigos[i].sprite]);
                if (enemigos[i].vivo) {
                        glTranslatef(enemigos[i].enemigox + cambiox, enemigos[i].enemigoy+cambioy,0.0);
                        enemigo();
                }
                glPopMatrix();
                if(colision(enemigos[i].enemigox + cambiox, enemigos[i].enemigoy + cambioy, balax.balax, balax.balay+balax.velocidad) && balax.vivo && enemigos[i].vivo) {
                        std::cout << "colision" << '\n';
                        balax.vivo = false;
                        enemigos[i].vivo = false;
                }

                if (enemigos[i].enemigoy + cambioy < -0.6) {
                  exit(0);
                }


                        balaenemi.balax = enemigos[i].enemigox + cambiox;

                balaenemi.balay = enemigos[i].enemigoy + cambioy;

                if (enemigos[i].sprite == 3) {
                        if (balaenemi.vivo) {
                                glPushMatrix();
                                glTranslatef(balaenemi.balax,balaenemi.balay+balaenemi.velocidad - 0.1,0.0);
                                glBindTexture(GL_TEXTURE_2D, m_ObjetoTexturaUNO[4]);
                                bala();
                                glPopMatrix();
                                if(colision(jugadorx, -0.8, balaenemi.balax, balaenemi.balay+balaenemi.velocidad)) {
                                        exit(0);
                                }
                        }
                }

        }

        if (balax.vivo) {
                glPushMatrix();
                glBindTexture(GL_TEXTURE_2D, m_ObjetoTexturaUNO[4]);
                glTranslatef(balax.balax,balax.balay+balax.velocidad,0.0);
                bala();
                glPopMatrix();
        }

        glPushMatrix();
        glBindTexture(GL_TEXTURE_2D, m_ObjetoTexturaUNO[0]);
        glTranslatef(jugadorx,-0.8,0.0);
        jugador();
        glPopMatrix();


        glutSwapBuffers();

        ///Siempre al final
        deltaTime = TiempoStart - TiempoViejo;
        TiempoViejo = TiempoStart;
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                if (jugadorx > ballXmin) {
                        direccionx -=0.015;
                }
                break;
        case 's':
        case 'S':
                balax.vivo = false;
                break;

        case 'd':
        case 'D':
                if (jugadorx < ballXmax) {
                        direccionx +=0.015;
                }
                break;
        case 'w':
        case 'W':
                if (balax.vivo==false) {
                        balax.balax = jugadorx;
                        balax.balay = -0.8;
                        balax.velocidad = 0.2;
                        balax.vivo = true;
                }
                break;
        case 'q':
        case 'Q':
                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                if (direccionx < 0) {
                        direccionx = 0.0;
                }
                break;
        case 'd':
        case 'D':
                if (direccionx > 0) {
                        direccionx = 0.0;
                }

                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                break;
        }
}


void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:
                        std::cout << x << "  "<< y << std::endl;
                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Space Invaders");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        for(int i=0; i<5; i++) {
                glDeleteTextures(5, &m_ObjetoTexturaUNO[i]);
                m_TexturaUNO->Release();
                delete m_TexturaUNO;
        }

        return 0;
}

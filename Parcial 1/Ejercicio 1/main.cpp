//#include <windows.h>  // For MS Windows
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
#include <GL/gl.h>
#include <GL/glu.h>

void Triangulo(){
  glBegin(GL_TRIANGLES);
    glVertex2f(0.0, 0.0);
    glVertex2f(0.4, 0.0);
    glVertex2f(0.2, 0.4);
  glEnd();
}

void display()
{
   glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Set background color to black and opaque
   glClear(GL_COLOR_BUFFER_BIT);         // Clear the color buffer
   Triangulo();
   glPushMatrix();
    glTranslatef(0.0, 0.8, 0.0);
    glRotatef(180, 1.0, 0.0, 0.0);
    glColor3f(0.0, 1.0, 0.0);
    Triangulo();
   glPopMatrix();

   glPushMatrix();
    glTranslatef(0.6, 0.2, 0.0);
    glRotatef(90, 0.0, 0.0, 1.0);
    glColor3f(1.0, 0.0, 0.0);
    Triangulo();
   glPopMatrix();

   glPushMatrix();
    glTranslatef(-0.2, 0.6, 0.0);
    glRotatef(270, 0.0, 0.0, 1.0);
    glColor3f(0.0, 0.0, 1.0);
    Triangulo();
   glPopMatrix();

   glFlush();  // Render now
}

/* Main function: GLUT runs as a console application starting at main()  */
int main(int argc, char** argv)
{
   glutInit(&argc, argv);                 // Initialize GLUT
   glutCreateWindow("Ejercicio OpenGL"); // Create a window with the given title
   glutInitWindowPosition(50, 50); // Position the window's initial top-left corner
   glutInitWindowSize(640, 640);   // Set the window's initial width & height
   glutDisplayFunc(display); // Register display callback handler for window re-paint
   glutMainLoop();           // Enter the infinitely event-processing loop
   return 0;
}

#include <iostream>
#include <stdio.h>
#include <time.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>  // GLUT, includes glu.h and gl.h
//-lglut -IGLU -IGL

void inicializar(void);
using namespace std;
int W=400, H=400;

bool MC[5][5][5] = {false};
int mSize = 5;
float cubeSize = 3.0f;

int TimeOld = 0;
float deltaTime =  0.0f;
int TimeStart;
float C = 10;
int i = 0;
int j =0;
int k = 0;
bool cambio1 = true;
bool cambio2 = false;
bool cambio3 = false;
bool cambio4 = false;
bool cambio5 = false;
bool cambio6 = false;
bool cambio7 = false;
bool cambio8 = false;
bool cambio9 = false;

void reshape(int w, int h)
{
        H=(h==0) ? 1 : h;
        W=(w==0) ? 1 : w;

        glViewport(0, 0, W, H);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        //glOrtho(-1.0, 1.0, -1.0, 1.0, 1.0, -1.0);
        gluPerspective(40.0f, (GLfloat)W/(GLfloat)H, 1.0f, 1000.0f);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
}

void inicializar(void)
{
        glClearColor(0.0, 0.0, 0.0, 0.0);
        reshape(W, H);
        glEnable(GL_DEPTH_TEST); //Profundidad

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        TimeOld = glutGet(GLUT_ELAPSED_TIME);
}

void Idle(void)
{
        TimeStart = glutGet(GLUT_ELAPSED_TIME);

        C -= deltaTime;
        glutPostRedisplay();

}

void ar() {
        if (cambio9) {
                cambio7=false;
                if(C >= 0) {
                        for (int u = 4; u >= 0; u--) {
                                for (int v = 4; v >= 0; v--) {
                                                MC[u][j][j] = false;
                                }
                        }
                          j--;
                          std::cout << j << '\n';
                          if (j==-5) {
                            cambio9=false;
                          }
                }
        }
}


void pr() {
        if (cambio7) {
                cambio6=false;
                if(C >= 0) {
                        for (int u = 0; u < 5; u++) {
                                for (int v = 0; v < 5; v++) {
                                                MC[u][j][j] = true;
                                }
                        }
                          j++;
                          std::cout << j << '\n';
                          if (j == 12) {
                            cambio9=true;
                          }
                }
        }
}




void ax() {
        if (cambio6) {
                cambio5=false;
                if(C >= 0) {
                        for (int u = 4; u >= 0; u--) {
                                for (int v = 4; v >= 0; v--) {
                                                MC[u][v][i] = false;
                                }
                        }
                          i--;
                          if (i==-15) {
                            cambio7=true;
                          }
                }
        }
}


void px() {
        if (cambio5) {
                cambio4=false;
                if(C <= 0) {
                        for (int u = 0; u < 5; u++) {
                                for (int v = 0; v < 5; v++) {
                                                MC[u][v][i] = true;

                                }
                        }
                          i++;
                          if (i ==11) {
                            cambio6=true;
                          }
                }
        }
}

void ay() {
        if (cambio4) {
                cambio3=false;
                if(C <= 0) {
                        MC[i][k][k] = false;
                        k--;
                        if(k==0) {
                                cambio5=true;
                        }
                }
        }
}


void py() {
        if (cambio3==true) {
                cambio2=false;
                if(C <= 0) {
                        MC[i][k][k] = true;
                        k++;
                        if (k==25) {
                                cambio4 = true;
                        }
                }
        }
}




void az() {
        if (cambio2) {
                cambio1=false;
                if(C <= 0) {
                        MC[i][j][k] = false;
                        k--;
                        if(k==0) {
                                cambio3=true;
                        }
                }
        }
}


void pz() {
        if (cambio1) {
                if(C <= 0) {
                        MC[i][j][k] = true;
                        k++;
                        if (k==125) {
                                cambio2 = true;
                        }
                }
        }
}



void render(void)
{
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();

        gluLookAt(-6.0f, 9.0f, -6.0f,
                  0.0f, 4.0f, 0.0f,
                  0.0f, 10.0f, 0.0f);

        //glRotatef(30.0, 1.0, 1.0, 1.0);

        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        glPointSize(1.0f);
        pz();
        az();
        py();
        ay();
        px();
        ax();
        pr();
        ar();

        for (int z = 0; z < mSize; z++)
        {
                for (int y = 0; y < mSize; y++)
                {
                        for (int x = 0; x < mSize; x++)
                        {
                                if(MC[x][y][z])
                                {
                                        glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                                        glPointSize(2.0f);
                                        glBegin(GL_POINTS);
                                        glVertex3f(x/(mSize/cubeSize), y/(mSize/cubeSize), z/(mSize/cubeSize));
                                        glEnd();
                                        glPointSize(1.0f);
                                } else {
                                        glColor4f(1.0f, 1.0f, 1.0f, 0.2f);
                                        glBegin(GL_POINTS);
                                        glVertex3f(x/(mSize/cubeSize), y/(mSize/cubeSize), z/(mSize/cubeSize));
                                        glEnd();
                                }
                        }
                }
        }

        deltaTime = TimeStart - TimeOld;
        TimeOld = TimeStart;

        glutSwapBuffers();
}

void eventosTeclado(unsigned char key, int x, int y)
{
        //estados_teclas[key]=true;
        switch(key)
        {
        case 'a':
        case 'A':
                //que hace con a
                break;

        case 'q':
        case 'Q':
                exit(0);
        }
}

void eventosTecladoUp(unsigned char key, int x, int y)
{
        //estados_teclas[key]=false;
        switch(key)
        {
        case 'a':
        case 'A':
                //que ahce
                break;
        }
}

void eventosTecladoEspecial(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

void eventosTecladoEspecialUp(int key, int x, int y)
{
        switch(key)
        {
        case GLUT_KEY_F1:
                //que ahce al presionar f1
                break;
        }
}

/*void analizar_teclas(void)
   {
    if (estados_teclas['A']==true)
        //Que hace al presionar A  y si quieres otros teclados al mismo tiempo
   }*/

void eventosMouse(int button, int state, int x, int y)
{
        switch(state)
        {
        case GLUT_UP:
                switch(button)
                {
                case GLUT_LEFT_BUTTON:
                        std::cout << x << "  "<< y << std::endl;
                        break;
                case GLUT_RIGHT_BUTTON:
                        break;
                case GLUT_MIDDLE_BUTTON:
                        break;
                }
                break;
        case GLUT_DOWN:
                break;
        }
}

int main(int argc, char **argv)
{
        //inicamos el glut
        glutInit(&argc, argv);
        //Especificamos el modo de display
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
        //Creamos la ventana
        glutInitWindowSize(800, 600);
        glutInitWindowPosition(10, 150);
        glutCreateWindow("Aplicacion OpenGL con GLUT");

        glutIdleFunc(Idle);
        glutDisplayFunc(render);
        glutReshapeFunc(reshape);
        inicializar();

        ///Teclados y mouse
        //Los que revisan teclado
        glutKeyboardFunc(eventosTeclado);
        glutKeyboardUpFunc(eventosTecladoUp);
        glutSpecialFunc(eventosTecladoEspecial);
        glutSpecialUpFunc(eventosTecladoEspecialUp);
        //mouse
        glutMouseFunc(eventosMouse);

        //El ciclo infinito
        glutMainLoop(); //se encargara de llamar la funcion de glutdisplayfunc,, en este caso es dibujar

        return 0;
}
